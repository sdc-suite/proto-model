import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    `java-library`
    kotlin("jvm")
}

val jdkVersion: JvmTarget = JvmTarget.JVM_17

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(jdkVersion.target)
    }
    withSourcesJar()
}

dependencies {
    testImplementation(kotlin("test"))
}

kotlin {
    // todo enable explicit api
    // explicitApi()
    compilerOptions {
        jvmTarget = jdkVersion
    }
}