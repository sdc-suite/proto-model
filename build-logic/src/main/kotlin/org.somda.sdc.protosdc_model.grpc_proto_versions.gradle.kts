import java.io.File
import java.util.*

plugins {
    id("com.google.protobuf")
}

// protobuf compiler version

val protobufVersionProps = Properties().apply {
    load(File(rootProject.projectDir, "./config/protobuf_version.txt").inputStream())
}

val protocVersion by extra(protobufVersionProps["protobuf_java_version"])


// grpc versions

val grpcVersionProps = Properties().apply {
    load(File(rootProject.projectDir, "./config/grpc_version.txt").inputStream())
}

val grpcJavaVersion by extra(grpcVersionProps["grpc_java_version"])
val grpcKotlinVersion by extra(grpcVersionProps["grpc_kotlin_version"])
