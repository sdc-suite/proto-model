import com.google.protobuf.gradle.*

plugins {
    id("org.somda.sdc.protosdc_model.shared")
    id("org.somda.sdc.protosdc_model.kotlin_library")
    id("org.somda.sdc.protosdc_model.grpc_proto_versions")
    id("org.somda.sdc.protosdc_model.grpc_workaround")
    id(libs.plugins.org.somda.gitlab.maven.publishing.get().pluginId)
}

val protocVersion: String by extra
val grpcJavaVersion: String by extra
val grpcKotlinVersion: String by extra

val osClassification: String by extra

dependencies {
    api(libs.kotlinx.coroutines)

    // protobuf and grpc lib versions are stored centrally in a separate config file, hence no toml entry to refer to

    implementation(group = "com.google.protobuf", name = "protobuf-java", version = protocVersion)
    implementation(group = "io.grpc", name = "grpc-stub", version = grpcJavaVersion)
    implementation(group = "io.grpc", name = "grpc-kotlin-stub", version = grpcKotlinVersion)
    implementation(group = "io.grpc", name = "grpc-protobuf", version = grpcJavaVersion)
}

tasks.named("processResources") {
    dependsOn(parent!!.tasks.named("copyProtoToKotlin"))
}

tasks.named("generateProto") {
    dependsOn(parent!!.tasks.named("copyProtoToKotlin"))
}

protobuf {
    protoc {
        // the artifact spec for the protobuf compiler
        artifact = "com.google.protobuf:protoc:$protocVersion:$osClassification"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcJavaVersion:$osClassification"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:$grpcKotlinVersion:jdk8@jar"
        }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                // apply the "grpc" plugin whose spec is defined above, without options.
                id("grpc")
                id("grpckt")
            }
        }
    }
}