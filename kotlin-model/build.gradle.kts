plugins {
    id("org.somda.sdc.protosdc_model.shared")
    id("org.somda.sdc.protosdc_model.kotlin_library")
    id(libs.plugins.org.somda.gitlab.maven.publishing.get().pluginId)
}