import com.google.protobuf.gradle.*
import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    java
    id("org.somda.sdc.protosdc_model.shared")
    id("org.somda.sdc.protosdc_model.grpc_proto_versions")
    id("org.somda.sdc.protosdc_model.grpc_workaround")
    id(libs.plugins.org.somda.gitlab.maven.publishing.get().pluginId)
}

val protocVersion: String by extra
val grpcJavaVersion: String by extra

val osClassification: String by extra

dependencies {
    // protobuf and grpc lib versions are stored centrally in a separate config file, hence no toml entry to refer to
    implementation(group = "com.google.protobuf", name = "protobuf-java", version = protocVersion)
    implementation(group = "io.grpc", name = "grpc-stub", version = grpcJavaVersion)
    implementation(group = "io.grpc", name = "grpc-protobuf", version = grpcJavaVersion)
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(JvmTarget.JVM_17.target)
    }
    withJavadocJar()
    withSourcesJar()
}

tasks.named("processResources") {
    dependsOn(parent!!.tasks.named("copyProtoToJava"))
}

tasks.named("generateProto") {
    dependsOn(parent!!.tasks.named("copyProtoToJava"))
}

protobuf {
    protoc {
        // the artifact spec for the Protobuf Compiler
        artifact = "com.google.protobuf:protoc:$protocVersion:$osClassification"
    }
    plugins {
        // optional: an artifact spec for a protoc plugin, with "grpc" as
        // the identifier, which can be referred to in the "plugins"
        // container of the "generateProtoTasks" closure.
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcJavaVersion:$osClassification"
        }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                // apply the "grpc" plugin whose spec is defined above, without options.
                id("grpc")
            }
        }
    }
}