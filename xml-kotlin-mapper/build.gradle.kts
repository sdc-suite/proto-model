plugins {
    id("org.somda.sdc.protosdc_model.shared")
    id("org.somda.sdc.protosdc_model.kotlin_library")
    id("org.somda.sdc.protosdc_model.grpc_proto_versions")
    id(libs.plugins.org.somda.gitlab.maven.publishing.get().pluginId)
}

val protocVersion: String by extra

dependencies {
    // protobuf and grpc lib versions are stored centrally in a separate config file, hence no toml entry to refer to
    implementation(group = "com.google.protobuf", name = "protobuf-java", version = protocVersion)

    implementation(libs.protosdc.converter.mappers.xml)

    implementation(projects.bicepsModelKt)
    implementation(projects.protosdcModelKt)
    implementation(projects.protosdcModelJava)
}