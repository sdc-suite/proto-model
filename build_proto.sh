#!/bin/sh
base_version=$(cat config/base_version.txt)

if [[ -z "${CI_PIPELINE_IID}" ]]; then
  build_version=""
else
  build_version=".${CI_PIPELINE_IID}"
fi

version="$base_version$build_version"

short_file_name_prefix="proto-model"
file_name_suffix=".zip"
file_name="$short_file_name_prefix""_""$version$file_name_suffix"

echo "Version: $version"
echo "Filename: $file_name"

zip -q -r "$file_name" proto/

# deploy only if enabled
if [[ "$*" == *--deploy* ]]
then
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$file_name" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/$short_file_name_prefix/$version/$short_file_name_prefix$file_name_suffix"
fi