extern crate proc_macro;

use crate::constants::{BOXED, CUSTOM_TARGET, FIELD_NAME, MODULE_PATH, PRIMITIVE, VARIANT};
use anyhow::{bail, Error};
use convert_case::{Case, Casing};
use log::trace;
use proc_macro2::{Ident, Span, TokenStream};
use quote::quote;
use syn::{Attribute, Data, DataStruct, DeriveInput, Fields, FieldsNamed, FieldsUnnamed, Type};

use crate::field::{filter_protosdc_attributes, str_attr, word_attr, Field};

mod constants;
mod field;

fn find_keyword_annotation(keyword_name: &str, attrs: &[Attribute]) -> Vec<String> {
    filter_protosdc_attributes(attrs)
        .iter()
        .filter_map(|it| {
            if let Ok(s) = str_attr(it, keyword_name) {
                s
            } else {
                None
            }
        })
        .collect()
}

fn has_name_annotation(name: &str, attrs: &[Attribute]) -> bool {
    filter_protosdc_attributes(attrs)
        .iter()
        .any(|it| word_attr(name, it))
}

fn find_mandatory_annotation(type_name: &str, attrs: &[Attribute]) -> Result<String, Error> {
    let mut proto_types = find_keyword_annotation(type_name, attrs);

    if proto_types.len() != 1 {
        bail!("Require exactly one {}", type_name)
    }

    Ok(proto_types.pop().unwrap())
}

fn find_mandatory_annotated_type(type_name: &str, attrs: &[Attribute]) -> Result<Type, Error> {
    Ok(syn::parse_str(
        find_mandatory_annotation(type_name, attrs)?.as_str(),
    )?)
}

fn find_proto_type(attrs: &[Attribute]) -> Result<Type, Error> {
    find_mandatory_annotated_type("proto_type", attrs)
}

fn try_protosdc_type(input: TokenStream) -> Result<TokenStream, Error> {
    let input: DeriveInput = syn::parse2(input)?;

    let ident = input.ident;

    let proto_type = find_proto_type(&input.attrs)?;

    let variant_data = match input.data {
        Data::Struct(variant_data) => variant_data,
        Data::Enum(..) => bail!("Message can not be derived for an enum"),
        Data::Union(..) => bail!("Message can not be derived for a union"),
    };

    trace!("struct attributes: {:?}", &input.attrs);

    let generics = &input.generics;
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    trace!("impl_generics: {:?}", impl_generics);
    trace!("ty_generics: {:?}", ty_generics);
    trace!("where_clause: {:?}", where_clause);

    let fields = match variant_data {
        DataStruct {
            fields: Fields::Named(FieldsNamed { named: fields, .. }),
            ..
        }
        | DataStruct {
            fields:
                Fields::Unnamed(FieldsUnnamed {
                    unnamed: fields, ..
                }),
            ..
        } => fields.into_iter().collect(),
        DataStruct {
            fields: Fields::Unit,
            ..
        } => Vec::new(),
    };

    let fields = fields
        .into_iter()
        .enumerate()
        .flat_map(|(_, field)| {
            let field_ident = field.ident.unwrap();

            trace!("{:?} with {:?}", field_ident, field.attrs);

            match Field::new(field_ident.to_string(), &field.attrs) {
                Ok(Some(field)) => Some(Ok((field_ident, field))),
                Ok(None) => None,
                Err(err) => Some(Err(
                    err.context(format!("invalid message field {}.{}", ident, field_ident))
                )),
            }
        })
        .collect::<Result<Vec<_>, Error>>()?;

    let proto_type_fields: Vec<TokenStream> = fields
        .iter()
        .map(|&(ref _field_ident, ref field)| field.generate_to_proto())
        .collect();

    let from_proto_fields: Vec<TokenStream> = fields
        .iter()
        .map(|&(ref _field_ident, ref field)| field.generate_from_proto())
        .collect();

    let struct_name = if fields.is_empty() {
        quote!()
    } else {
        quote!(
            const STRUCT_NAME: &'static str = stringify!(#ident);
        )
    };

    trace!("struct: {:?} fields: {:?}", &struct_name, &fields);

    let expanded = quote! {
        impl #impl_generics ::protosdc_mapping::ProtoSdcType<#proto_type> for #ident #ty_generics #where_clause {}

        impl #impl_generics ::protosdc_mapping::ProtoSdcMessage<#proto_type> for #ident #ty_generics #where_clause {}

        impl #impl_generics Into<#proto_type> for #ident #ty_generics #where_clause {
            fn into(self) -> #proto_type {
                #proto_type {
                    #(#proto_type_fields),*
                }
            }
        }

        impl #impl_generics TryFrom<#proto_type> for #ident #ty_generics #where_clause {
            type Error = ::protosdc_mapping::MappingError;
            fn try_from(value: #proto_type) -> Result<Self, Self::Error> {
                Ok(Self {
                    #(#from_proto_fields),*
                })
            }
        }
    };

    Ok(expanded)
}

#[proc_macro_derive(protoSDCType, attributes(protosdc))]
pub fn protosdc_type(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    try_protosdc_type(input.into()).unwrap().into()
}

fn try_protosdc_enumeration(input: TokenStream) -> Result<TokenStream, Error> {
    let input: DeriveInput = syn::parse2(input)?;
    let ident = input.ident;

    let proto_type = find_proto_type(&input.attrs)?;

    let variant_data = match input.data {
        Data::Struct(..) => bail!("Enum can not be derived for a struct"),
        Data::Enum(variant_data) => variant_data,
        Data::Union(..) => bail!("Enum can not be derived for a union"),
    };

    trace!("enum attributes: {:?}", &input.attrs);

    let generics = &input.generics;
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let variant_idents: Vec<(String, Ident)> = variant_data
        .variants
        .into_iter()
        .map(
            |rust_variant| match find_mandatory_annotation(VARIANT, &rust_variant.attrs) {
                Ok(proto_variant) => Ok((proto_variant, rust_variant.ident)),
                Err(err) => Err(err),
            },
        )
        .collect::<Result<Vec<_>, Error>>()?;

    let variant_idents: Vec<(Ident, Ident)> = variant_idents
        .into_iter()
        .map(|(proto_variant, rust_variant)| {
            (
                Ident::new(&apply_proto_enum_rules(proto_variant), Span::call_site()),
                rust_variant,
            )
        })
        .collect();

    trace!("Variants: {:?}", variant_idents);

    let from_proto_fields: Vec<TokenStream> = variant_idents
        .iter()
        .map(|(proto_variant, rust_variant)| quote! { #proto_type::#proto_variant => #ident::#rust_variant })
        .collect();

    let proto_type_fields: Vec<TokenStream> = variant_idents
        .iter()
        .map(|(proto_variant, rust_variant)| quote! { #ident::#rust_variant => #proto_type::#proto_variant })
        .collect();

    let expanded = quote! {
        impl #impl_generics ::protosdc_mapping::ProtoSdcType<#proto_type> for #ident #ty_generics #where_clause {
        }

        impl #impl_generics ::protosdc_mapping::ProtoSdcEnum<#proto_type> for #ident #ty_generics #where_clause {}


        impl #impl_generics Into<#proto_type> for #ident #ty_generics #where_clause {
            fn into(self) -> #proto_type {
                match self {
                    #(#proto_type_fields),*
                }
            }
        }

        impl #impl_generics TryFrom<#proto_type> for #ident #ty_generics #where_clause {
            type Error = ::protosdc_mapping::MappingError;
            fn try_from(value: #proto_type) -> Result<Self, Self::Error> {
                Ok(match value {
                    #(#from_proto_fields),*
                })
            }
        }
    };

    Ok(expanded)
}

#[proc_macro_derive(protoSDCEnumeration, attributes(protosdc))]
pub fn protosdc_enumeration(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    try_protosdc_enumeration(input.into()).unwrap().into()
}

fn apply_proto_enum_rules(variant: String) -> String {
    variant.to_case(Case::Pascal)
}

fn try_protosdc_one_of(input: TokenStream) -> Result<TokenStream, Error> {
    let input: DeriveInput = syn::parse2(input)?;
    let ident = input.ident;

    let proto_type = find_proto_type(&input.attrs)?;
    let field_name = find_mandatory_annotated_type(FIELD_NAME, &input.attrs)?;
    let mod_path = find_mandatory_annotated_type(MODULE_PATH, &input.attrs)?;

    let variant_data = match input.data {
        Data::Struct(..) => bail!("Enum can not be derived for a struct"),
        Data::Enum(variant_data) => variant_data,
        Data::Union(..) => bail!("Enum can not be derived for a union"),
    };

    let variant_idents: Vec<(bool, bool, bool, Ident)> = variant_data
        .variants
        .into_iter()
        .map(|it| {
            (
                has_name_annotation(PRIMITIVE, &it.attrs),
                has_name_annotation(BOXED, &it.attrs),
                has_name_annotation(CUSTOM_TARGET, &it.attrs),
                it.ident,
            )
        })
        .collect();

    trace!("Variants: {:?}", variant_idents);
    trace!("oneof attributes: {:?}", &input.attrs);

    let generics = &input.generics;
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let from_proto_fields: Vec<TokenStream> = variant_idents
        .iter()
        .map(|(primitive, boxed, custom_target, variant_ident)| match (primitive, boxed, custom_target) {
            (false, false, _) => quote! { Some(#mod_path::#variant_ident(val)) => Self::#variant_ident(val.try_into()?) },
            (true, false, false) => quote! { Some(#mod_path::#variant_ident(val)) => Self::#variant_ident(val.into()) },
            (true, false, true) => quote! { Some(#mod_path::#variant_ident(val)) => Self::#variant_ident(val.try_into()?) },
            (true, true, _) => quote! { Some(#mod_path::#variant_ident(val)) => Self::#variant_ident(Box::new((*val).into())) },
            (false, true, _) => quote! { Some(#mod_path::#variant_ident(val)) => Self::#variant_ident(Box::new((*val).try_into()?)) },
        })
        .collect();

    let proto_type_fields: Vec<TokenStream> = variant_idents
        .iter()
        .map(|(_, boxed, _, variant_ident)| match boxed {
            false => quote! { #ident::#variant_ident(val) => Some(#mod_path::#variant_ident(val.into()))},
            true => quote! { #ident::#variant_ident(val) => Some(#mod_path::#variant_ident(Box::new((*val).into())))},
        })
        .collect();

    let expanded = quote! {
        impl #impl_generics ::protosdc_mapping::ProtoSdcType<#proto_type> for #ident #ty_generics #where_clause {
        }

        impl #impl_generics ::protosdc_mapping::ProtoSdcOneOf<#proto_type> for #ident #ty_generics #where_clause {}


        impl #impl_generics Into<#proto_type> for #ident #ty_generics #where_clause {
            fn into(self) -> #proto_type {
                #proto_type {
                    #field_name: match self {
                        #(#proto_type_fields),*
                    }
                }
            }
        }

        impl #impl_generics TryFrom<#proto_type> for #ident #ty_generics #where_clause {
            type Error = ::protosdc_mapping::MappingError;
            fn try_from(value: #proto_type) -> Result<Self, Self::Error> {
                Ok(match value.#field_name {
                    #(#from_proto_fields),*,
                    None => return Err(Self::Error::FromProtoMandatoryMissing { field_name: stringify!(#proto_type.#field_name).to_string() })
                })
            }
        }
    };

    Ok(expanded)
}

#[proc_macro_derive(protoSDCOneOf, attributes(protosdc))]
pub fn protosdc_one_of(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    try_protosdc_one_of(input.into()).unwrap().into()
}
