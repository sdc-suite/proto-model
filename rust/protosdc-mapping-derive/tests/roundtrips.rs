#[derive(::prost::Message, Clone, PartialEq)]
pub struct TestStructProto {
    #[prost(string, tag = "1")]
    hello: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "2")]
    nested: ::core::option::Option<TestNestedStructProto>,
    #[prost(message, optional, boxed, tag = "3")]
    nested_boxed: ::core::option::Option<::prost::alloc::boxed::Box<TestNestedStructProto>>,
    #[prost(message, repeated, tag = "4")]
    repeated: Vec<TestNestedStructProto>,
}

#[derive(::prost::Message, Clone, PartialEq)]
pub struct TestNestedStructProto {
    #[prost(string, tag = "1")]
    test_field: ::prost::alloc::string::String,
}

#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MdsOperatingModeMsg {
    #[prost(enumeration = "mds_operating_mode_msg::EnumType", tag = "1")]
    pub enum_type: i32,
}
/// Nested message and enum types in `MdsOperatingModeMsg`.
pub mod mds_operating_mode_msg {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum EnumType {
        NMl = 0,
        Dmo = 1,
        Srv = 2,
        Mtn = 3,
    }
}

#[derive(protosdc_mapping_derive::protoSDCType, Clone, PartialEq, Debug)]
#[protosdc(proto_type = "crate::TestNestedStructProto")]
pub struct TestNestedStruct {
    #[protosdc(primitive, field_name = "test_field")]
    test_field_x: String,
}

#[derive(protosdc_mapping_derive::protoSDCType, Clone, PartialEq, Debug)]
#[protosdc(proto_type = "crate::TestStructProto")]
pub struct TestStruct {
    #[protosdc(primitive)]
    hello: String,
    nested: TestNestedStruct,
    #[protosdc(boxed)]
    nested_boxed: Box<TestNestedStruct>,
    #[protosdc(repeated)]
    repeated: Vec<TestNestedStruct>,
}

#[derive(protosdc_mapping_derive::protoSDCType, Clone, PartialEq, Debug)]
#[protosdc(proto_type = "crate::MdsOperatingModeMsg")]
pub struct MdsOperatingMode {
    #[protosdc(enumeration = "crate::mds_operating_mode_msg::EnumType")]
    pub enum_type: mds_operating_mode_mod::EnumType,
}

pub mod mds_operating_mode_mod {
    #[allow(non_camel_case_types)]
    #[derive(protosdc_mapping_derive::protoSDCEnumeration, Clone, PartialEq, Debug)]
    #[protosdc(proto_type = "crate::mds_operating_mode_msg::EnumType")]
    pub enum EnumType {
        #[protosdc(variant = "N_Ml")]
        Nml,
        #[protosdc(variant = "Dmo")]
        Dmo,
        #[protosdc(variant = "Srv")]
        Srv,
        #[protosdc(variant = "Mtn")]
        Mtn,
    }
}

#[derive(Clone, PartialEq, ::prost::Message)]
pub struct TestStructOneOfMsg {
    #[prost(oneof = "test_struct_one_of_msg::TestStructOneOf", tags = "1, 2")]
    pub test_struct_one_of: ::core::option::Option<test_struct_one_of_msg::TestStructOneOf>,
}

pub mod test_struct_one_of_msg {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum TestStructOneOf {
        #[prost(message, tag = "1")]
        TestStruct(crate::TestStructProto),
        #[prost(message, tag = "2")]
        TestNestedStruct(crate::TestNestedStructProto),
    }
}

#[derive(Clone, PartialEq, ::protosdc_mapping_derive::protoSDCOneOf, Debug)]
#[protosdc(
    proto_type = "crate::TestStructOneOfMsg",
    field_name = "test_struct_one_of",
    mod_path = "crate::test_struct_one_of_msg::TestStructOneOf"
)]
pub enum TestStructOneOf {
    TestStruct(crate::TestStruct),
    TestNestedStruct(crate::TestNestedStruct),
}

#[test]
fn test_message_round_trip() {
    let data = TestStruct {
        hello: "bye".to_string(),
        nested: TestNestedStruct {
            test_field_x: "tested".to_string(),
        },
        nested_boxed: Box::new(TestNestedStruct {
            test_field_x: "boxed".to_string(),
        }),
        repeated: vec![
            TestNestedStruct {
                test_field_x: "vec1".to_string(),
            },
            TestNestedStruct {
                test_field_x: "vec2".to_string(),
            },
        ],
    };

    let converted: TestStructProto = data.clone().into();
    assert_eq!(data.hello, converted.hello);
    assert_eq!(
        data.nested.test_field_x,
        converted.nested.as_ref().unwrap().test_field
    );
    assert_eq!(
        data.nested_boxed.test_field_x,
        converted.nested_boxed.as_ref().unwrap().test_field
    );
    let returned: TestStruct = converted.try_into().expect("Could not convert into rust");
    assert_eq!(data.hello, returned.hello);
    assert_eq!(data.nested.test_field_x, returned.nested.test_field_x);
    assert_eq!(
        data.nested_boxed.test_field_x,
        returned.nested_boxed.test_field_x
    );

    assert_eq!(2, data.repeated.len());

    assert_eq!(data, returned)
}

#[test]
fn test_enum() {
    let op_mode = MdsOperatingModeMsg { enum_type: 2 };

    let op_mod_rs: MdsOperatingMode = op_mode.clone().try_into().unwrap();
    let op_mode_back = op_mod_rs.into();

    assert_eq!(op_mode, op_mode_back)
}

#[test]
fn test_oneof() {
    let of = TestStructOneOf::TestNestedStruct(TestNestedStruct {
        test_field_x: "TestNestedStruct::TestNestedStruct::test_field_x".to_string(),
    });

    assert!(matches!(of, TestStructOneOf::TestNestedStruct(..)));

    let proto: TestStructOneOfMsg = of.clone().into();
    let of_back: TestStructOneOf = proto.try_into().expect("Could not go back");

    assert_eq!(of, of_back);
    assert!(matches!(of_back, TestStructOneOf::TestNestedStruct(..)));
}
