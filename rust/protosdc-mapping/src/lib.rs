use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Error)]
pub enum MappingError {
    #[error("An error occurred mapping from protobuf to rust: {message}")]
    FromProtoGeneric { message: String },
    #[error("A mandatory field is missing in protobuf: {field_name}")]
    FromProtoMandatoryMissing { field_name: String },
    #[error(transparent)]
    DecodeError(prost::DecodeError),
}

pub trait ProtoSdcType<T>: Clone + PartialEq + TryFrom<T, Error = MappingError> + Into<T> {}

pub trait ProtoSdcMessage<T>:
    Clone + PartialEq + TryFrom<T, Error = MappingError> + ProtoSdcType<T>
where
    T: prost::Message,
{
}

pub trait ProtoSdcEnum<T>:
    Clone + PartialEq + TryFrom<T, Error = MappingError> + ProtoSdcType<T>
{
}

pub trait ProtoSdcOneOf<T>:
    Clone + PartialEq + TryFrom<T, Error = MappingError> + ProtoSdcType<T>
{
}
