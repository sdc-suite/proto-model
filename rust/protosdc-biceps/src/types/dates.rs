use crate::types::dates_common::{
    parse_i32, parse_u32, DATE_TIME_PARSER, DAY_GROUP, HOUR_GROUP, MILLISECOND_GROUP, MINUTE_GROUP,
    MONTH_GROUP, SECOND_GROUP, TZ_HOURS_GROUP, TZ_MINUTES_GROUP, TZ_SIGN_GROUP, TZ_UTC_GROUP,
    YEAR_GROUP, YEAR_MONTH_PARSER, YEAR_PARSER, YEAR_SIGN_GROUP,
};

use chrono::{DateTime, FixedOffset, NaiveDate, NaiveDateTime, Utc};
use protosdc_mapping::{MappingError, ProtoSdcType};
use std::ops::Deref;

#[derive(Debug, Clone, PartialEq)]
pub enum DateTimeEnum {
    // when we received a timezone, converted to UTC
    DateTime(DateTime<Utc>),
    // no timezone info present
    NaiveDateTime(NaiveDateTime),
}

#[derive(Debug, Clone, PartialEq)]
pub struct ProtoDateTime {
    pub date_time: DateTimeEnum,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ProtoYear {
    pub year: i32,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ProtoYearMonth {
    pub year: i32,
    pub month: u32,
}

impl Deref for ProtoDateTime {
    type Target = DateTimeEnum;
    fn deref(&self) -> &Self::Target {
        &self.date_time
    }
}
impl Deref for ProtoYear {
    type Target = i32;
    fn deref(&self) -> &Self::Target {
        &self.year
    }
}

fn parse_date_time_regex(value: &str) -> Result<DateTimeEnum, MappingError> {
    match DATE_TIME_PARSER.captures(&value) {
        Some(capture) => {
            let year_sign = capture.name(YEAR_SIGN_GROUP);
            let year_raw: i32 = parse_i32(&capture.name(YEAR_GROUP), "dateTime", "year")?;
            let year = match year_sign {
                None => year_raw,
                Some(_) => -year_raw,
            };
            let month: u32 = parse_u32(&capture.name(MONTH_GROUP), "dateTime", "month")?;
            let day: u32 = parse_u32(&capture.name(DAY_GROUP), "dateTime", "day")?;

            let tz_sign_raw = capture.name(TZ_SIGN_GROUP);
            let tz_hours_raw = capture.name(TZ_HOURS_GROUP);
            let tz_minutes_raw = capture.name(TZ_MINUTES_GROUP);
            let tz_utc_raw = capture.name(TZ_UTC_GROUP);

            let naive_date: NaiveDate = NaiveDate::from_ymd_opt(year, month, day).ok_or(
                MappingError::FromProtoGeneric {
                    message: format!(
                        "{}:{}:{} was not a valid input for year-month-day",
                        year, month, day
                    ),
                },
            )?;

            let hours_raw = &capture.name(HOUR_GROUP);
            let naive_date_time = if hours_raw.is_some() {
                let hours: u32 = parse_u32(&capture.name(HOUR_GROUP), "dateTime", "hours")?;
                let minutes: u32 = parse_u32(&capture.name(MINUTE_GROUP), "dateTime", "minutes")?;
                let mut seconds: u32 =
                    parse_u32(&capture.name(SECOND_GROUP), "dateTime", "seconds")?;

                let milliseconds_raw = capture.name(MILLISECOND_GROUP);
                let mut milliseconds: u32 = match milliseconds_raw {
                    None => 0,
                    Some(ms) => {
                        // count leading zeros
                        let mut leading: u32 = 0;
                        for c in ms.as_str().chars() {
                            match c {
                                '0' => leading += 1,
                                _ => break,
                            }
                        }
                        let millis_value: u32 =
                            ms.as_str()
                                .parse()
                                .map_err(|_| MappingError::FromProtoGeneric {
                                    message: "milliseconds could not be parsed to u32".to_string(),
                                })?;

                        millis_value * 10_u32.pow(leading)
                    }
                };

                // leap second handling
                if seconds == 60 {
                    seconds = 59;
                    milliseconds = milliseconds + 1000
                }

                naive_date
                    .and_hms_milli_opt(hours, minutes, seconds, milliseconds)
                    .ok_or(MappingError::FromProtoGeneric {
                        message: format!(
                            "{}.{}.{}.{} was not a valid input for time",
                            hours, minutes, seconds, milliseconds
                        ),
                    })?
            } else {
                // only 24:00:00 could've validated as an alternative
                naive_date
                    .and_hms_milli_opt(24, 0, 0, 0)
                    .ok_or(MappingError::FromProtoGeneric {
                        message: format!("24:00:00 was not a valid input for time"),
                    })?
            };

            if let (Some(tz_sign_match), Some(tz_hours_match), Some(tz_minutes_match)) =
                (tz_sign_raw, tz_hours_raw, tz_minutes_raw)
            {
                let tz_hours: i32 = tz_hours_match.as_str().parse().map_err(|_| {
                    MappingError::FromProtoGeneric {
                        message: "hours could not be parsed to i32".to_string(),
                    }
                })?;
                let tz_minutes: i32 = tz_minutes_match.as_str().parse().map_err(|_| {
                    MappingError::FromProtoGeneric {
                        message: "minutes could not be parsed to i32".to_string(),
                    }
                })?;

                let parsed_tz_opt: Option<FixedOffset> = match tz_sign_match.as_str() {
                    // correct time by adding the difference to utc
                    "-" => FixedOffset::east_opt(-(tz_hours * 3600 + tz_minutes * 60)),
                    // or subtract it, if it was added before
                    _ => FixedOffset::east_opt(tz_hours * 3600 + tz_minutes * 60),
                };

                let parsed_tz = if let Some(it) = parsed_tz_opt {
                    it
                } else {
                    return Err(MappingError::FromProtoGeneric {
                        message: "Parsed timezone was out of range".to_string(),
                    });
                };

                let date: DateTime<FixedOffset> = DateTime::from_local(naive_date_time, parsed_tz);
                let utc_date: DateTime<Utc> = DateTime::from_utc(date.naive_utc(), Utc);

                Ok(DateTimeEnum::DateTime(utc_date))
            } else if tz_utc_raw.is_some() {
                Ok(DateTimeEnum::DateTime(DateTime::from_utc(
                    naive_date_time,
                    Utc,
                )))
            } else {
                Ok(DateTimeEnum::NaiveDateTime(naive_date_time))
            }
        }
        None => Err(MappingError::FromProtoGeneric {
            message: format!("String {} was not a valid dateTime.", &value),
        }),
    }
}

impl TryFrom<String> for ProtoDateTime {
    type Error = MappingError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Ok(Self {
            date_time: parse_date_time_regex(&value)?,
        })
    }
}

impl Into<String> for ProtoDateTime {
    fn into(self) -> String {
        match self.date_time {
            DateTimeEnum::DateTime(d) => format!("{}", d.format("%Y-%m-%dT%H:%M:%S%.fZ")),
            DateTimeEnum::NaiveDateTime(d) => format!("{}", d.format("%Y-%m-%dT%H:%M:%S%.f")),
        }
    }
}

impl ProtoSdcType<String> for ProtoDateTime {}

impl TryFrom<String> for ProtoYear {
    type Error = MappingError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match YEAR_PARSER.captures(&value) {
            Some(capture) => {
                let year_sign = capture.name(YEAR_SIGN_GROUP);
                let year: i32 = parse_i32(&capture.name(YEAR_GROUP), "year", "year")?;

                match year_sign {
                    Some(_) => Ok(Self { year: -year }),
                    None => Ok(Self { year }),
                }
            }
            None => Err(MappingError::FromProtoGeneric {
                message: format!("String {} was not a valid year.", &value),
            }),
        }
    }
}

impl Into<String> for ProtoYear {
    fn into(self) -> String {
        format!("{:04}", self.year)
    }
}

impl ProtoSdcType<String> for ProtoYear {}

impl TryFrom<String> for ProtoYearMonth {
    type Error = MappingError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match YEAR_MONTH_PARSER.captures(&value) {
            Some(capture) => {
                let year_sign = capture.name(YEAR_SIGN_GROUP);
                let year: i32 = parse_i32(&capture.name(YEAR_GROUP), "year", "year_month")?;
                let month: u32 = parse_u32(&capture.name(MONTH_GROUP), "month", "year_month")?;

                match year_sign {
                    Some(_) => Ok(Self { year: -year, month }),
                    None => Ok(Self { year, month }),
                }
            }
            None => Err(MappingError::FromProtoGeneric {
                message: format!("String {} was not a valid year.", &value),
            }),
        }
    }
}

impl Into<String> for ProtoYearMonth {
    fn into(self) -> String {
        format!("{:04}-{:02}", self.year, self.month)
    }
}

impl ProtoSdcType<String> for ProtoYearMonth {}

#[cfg(test)]
mod test {
    use crate::types::dates::MappingError;
    use crate::types::{ProtoDateTime, ProtoYear, ProtoYearMonth};
    use anyhow::Result;

    #[test]
    fn valid_year_month() -> Result<()> {
        let valid_years = vec![
            ("2020-10", "2020-10"),
            ("-250142362-04", "-250142362-04"),
            ("1121122-01+05:10", "1121122-01"),
        ];

        for (year_month, expected) in valid_years {
            let result = ProtoYearMonth::try_from(year_month.to_string())?;
            let res_string: String = result.into();
            assert_eq!(expected, res_string)
        }
        Ok(())
    }

    #[test]
    fn invalid_year_month() -> Result<()> {
        let valid_years = vec!["2020-13", "-a250142362-04", "1121122-01-10", ""];

        for year_month in valid_years {
            let result = ProtoYearMonth::try_from(year_month.to_string());
            assert!(matches!(result, Err(MappingError::FromProtoGeneric { .. })));
        }
        Ok(())
    }

    #[test]
    fn valid_year() -> Result<()> {
        let valid_years = vec![
            ("2020", "2020"),
            ("-250142362", "-250142362"),
            ("1121122+05:10", "1121122"),
        ];

        for (year, expected) in valid_years {
            let result = ProtoYear::try_from(year.to_string())?;
            let res_string: String = result.into();
            assert_eq!(expected, res_string)
        }
        Ok(())
    }

    #[test]
    fn invalid_year() -> Result<()> {
        let invalid = vec![
            "a2020",
            "twenty-twenty",
            // should be invalid but we ignore tz data right now
            // "1121122+25:10"
            "",
        ];

        for year in invalid {
            let result = ProtoYear::try_from(year.to_string());
            assert!(matches!(result, Err(MappingError::FromProtoGeneric { .. })));
        }
        Ok(())
    }

    #[test]
    fn valid_date_time() -> Result<()> {
        let valid = vec![
            ("2002-10-10T12:00:00", "2002-10-10T12:00:00"),
            ("2002-10-10T12:00:00.05+03:00", "2002-10-10T09:00:00.050Z"),
            ("2002-10-10T12:00:00.05-03:00", "2002-10-10T15:00:00.050Z"),
            ("2002-10-10T12:00:00-05:00", "2002-10-10T17:00:00Z"),
            ("1996-12-19T16:39:57-08:00", "1996-12-20T00:39:57Z"),
            ("-0010-12-12T12:59:60Z", "-0010-12-12T12:59:60Z"),
        ];

        for (date_time, expected) in valid {
            let result = ProtoDateTime::try_from(date_time.to_string())?;
            let res_string: String = result.into();
            assert_eq!(expected, res_string)
        }

        Ok(())
    }

    #[test]
    fn invalid_date_time() -> Result<()> {
        let invalid = vec![
            "2002-10-10T12:00:61",
            // not invalid yet.
            // "2002-10-10T12:00:00.05+15:00",
            // "2002-10-10T09:00:00.05-15:00",
            "2002-10-10Z12:00:00.05-03:00",
        ];

        for value in invalid {
            let result = ProtoDateTime::try_from(value.to_string());
            assert!(
                matches!(result, Err(MappingError::FromProtoGeneric { .. })),
                "result: {:?}",
                result
            );
        }

        Ok(())
    }
}
