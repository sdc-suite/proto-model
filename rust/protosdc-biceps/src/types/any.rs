use dyn_clone::DynClone;
#[cfg(feature = "xml")]
use log::error;
use log::warn;
use prost::Message;
use prost_types::Any;
use protosdc_mapping::MappingError;
use protosdc_proto::biceps::RetrievabilityMsg;
#[cfg(feature = "sdpi")]
use protosdc_proto::extension::sdpi::{
    CodedAttributesMsg, EquipmentIdentifierMsg, GenderMsg,
};
#[cfg(feature = "xml")]
use protosdc_xml::{
    xml_reader::ReplacementExtensionHandler, ComplexXmlTypeWrite, ExtensionParserError,
    ExtensionType, GenericXmlReaderComplexTypeRead, QNameSlice, QNameStr, XmlReader,
};
#[cfg(feature = "xml")]
use quick_xml::name::{Namespace, ResolveResult};
#[cfg(feature = "xml")]
use std::borrow::Borrow;
use std::fmt::{Debug, Formatter};

use crate::biceps::Retrievability;
#[cfg(feature = "sdpi")]
use crate::extension::sdpi::{CodedAttributes, EquipmentIdentifier, Gender};

pub trait ToProstAnyMessage {
    fn to_prost(&self) -> prost_types::Any;
}

impl<T: ToProstAnyMessage> ToProstAnyMessage for Box<T> {
    fn to_prost(&self) -> Any {
        (**self).to_prost()
    }
}

impl ToProstAnyMessage for Retrievability {
    fn to_prost(&self) -> prost_types::Any {
        let ret: RetrievabilityMsg = self.clone().into();
        prost_types::Any {
            type_url: "org.somda.protosdc.proto.model.biceps.RetrievabilityMsg".to_string(),
            value: ret.encode_to_vec(),
        }
    }
}

#[cfg(feature = "sdpi")]
impl ToProstAnyMessage for CodedAttributes {
    fn to_prost(&self) -> Any {
        let ret: CodedAttributesMsg = self.clone().into();
        Any {
            type_url: "org.somda.protosdc.proto.model.extension.sdpi.CodedAttributesMsg"
                .to_string(),
            value: ret.encode_to_vec(),
        }
    }
}

#[cfg(feature = "sdpi")]
impl ToProstAnyMessage for Gender {
    fn to_prost(&self) -> Any {
        let ret: GenderMsg = self.clone().into();
        Any {
            type_url: "org.somda.protosdc.proto.model.extension.sdpi.GenderMsg".to_string(),
            value: ret.encode_to_vec(),
        }
    }
}

#[cfg(feature = "sdpi")]
impl ToProstAnyMessage for EquipmentIdentifier {
    fn to_prost(&self) -> Any {
        let ret: EquipmentIdentifierMsg = self.clone().into();
        Any {
            type_url: "org.somda.protosdc.proto.model.extension.sdpi.EquipmentIdentifierMsg"
                .to_string(),
            value: ret.encode_to_vec(),
        }
    }
}

#[cfg(feature = "xml")]
pub trait AnyContent: ToProstAnyMessage + ExtensionType + Debug + Send + Sync + DynClone {
    fn is_equal(&self, _: &dyn AnyContent) -> bool;
}
#[cfg(feature = "xml")]
impl<T> AnyContent for T
where
    T: ToProstAnyMessage + ExtensionType + Debug + Send + Sync + Clone + PartialEq + 'static,
{
    fn is_equal(&self, other: &dyn AnyContent) -> bool {
        // Do a type-safe casting. If the types are different,
        // return false, otherwise test the values for equality.
        other
            .as_any()
            .downcast_ref::<T>()
            .map_or(false, |a| self == a)
    }
}

#[cfg(not(feature = "xml"))]
pub trait AnyContent: ToProstAnyMessage + Debug + Send + Sync + DynClone {
    // An &Any can be cast to a reference to a concrete type.
    fn as_any(&self) -> &dyn core::any::Any;
    fn is_equal(&self, _: &dyn AnyContent) -> bool;
}
#[cfg(not(feature = "xml"))]
impl<T> AnyContent for T
where
    T: ToProstAnyMessage + Debug + Send + Sync + Clone + PartialEq + 'static,
{
    fn as_any(&self) -> &dyn core::any::Any {
        self
    }
    fn is_equal(&self, other: &dyn AnyContent) -> bool {
        // Do a type-safe casting. If the types are different,
        // return false, otherwise test the values for equality.
        other
            .as_any()
            .downcast_ref::<T>()
            .map_or(false, |a| self == a)
    }
}

dyn_clone::clone_trait_object!(AnyContent);

/// Custom any for later implementation.
pub struct ProtoAny {
    pub data: Box<dyn AnyContent + Send + Sync>,
}

impl Clone for ProtoAny {
    fn clone(&self) -> Self {
        Self {
            data: dyn_clone::clone_box(&*self.data),
        }
    }
}

impl PartialEq for ProtoAny {
    fn eq(&self, _other: &Self) -> bool {
        (*self.data).is_equal(&*_other.data)
    }
}

impl Debug for ProtoAny {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "ProtoAny")
    }
}

impl Into<prost_types::Any> for ProtoAny {
    fn into(self) -> prost_types::Any {
        self.data.to_prost()
    }
}

impl TryFrom<prost_types::Any> for ProtoAny {
    type Error = MappingError;

    fn try_from(value: prost_types::Any) -> Result<Self, Self::Error> {
        // match known types
        match value {
            prost_types::Any { type_url, value }
                if &type_url == "org.somda.protosdc.proto.model.biceps.RetrievabilityMsg" =>
            {
                let ret: Retrievability = RetrievabilityMsg::decode(&*value)
                    .map_err(MappingError::DecodeError)?
                    .try_into()?;

                Ok(Self {
                    data: Box::new(Box::new(ret)),
                })
            }
            _ => {
                warn!("Unknown Extension {}", &value.type_url);
                Ok(Self {
                    data: Box::new(Box::new(Retrievability { by: vec![] })),
                })
            }
        }
    }
}

#[cfg(feature = "xml")]
const EXTENSION_NAMESPACE: &'static [u8] =
    b"http://standards.ieee.org/downloads/11073/11073-10207-2017/extension";
#[cfg(feature = "xml")]
const MUST_UNDERSTAND: QNameSlice = (
    quick_xml::name::ResolveResult::Bound(quick_xml::name::Namespace(EXTENSION_NAMESPACE)),
    b"mustUnderstand",
);

#[cfg(feature = "xml")]
const TRUE: &'static [u8] = b"true";
#[cfg(feature = "xml")]
const TRUE_INT: &'static [u8] = b"1";
#[cfg(feature = "xml")]
const FALSE: &'static [u8] = b"false";
#[cfg(feature = "xml")]
const FALSE_INT: &'static [u8] = b"0";

#[cfg(feature = "xml")]
fn find_must_understand<B: ::std::io::BufRead, C: ExtensionType>(
    event: &quick_xml::events::BytesStart,
    reader: &mut XmlReader<B, C>,
) -> Result<bool, protosdc_xml::ParserError> {
    for attr in event.attributes() {
        let attribute = attr.map_err(|err| {
            protosdc_xml::ParserError::QuickXMLError(quick_xml::Error::InvalidAttr(err))
        })?;
        match attribute.key {
            _ => {
                // ignore xmlns attributes, they're namespace definitions
                if attribute.key.0.starts_with(b"xmlns:") {
                    continue;
                }
                // handle qualified namespace
                let resolved = reader.resolve_attribute(attribute.key);
                match (resolved.0, resolved.1.as_ref()) {
                    MUST_UNDERSTAND => {
                        // verify type is correct
                        return Ok(match attribute.value.borrow() {
                            TRUE => true,
                            TRUE_INT => true,
                            FALSE => false,
                            FALSE_INT => false,
                            _ => false,
                        });
                    }
                    _ => {} // fall through
                }
            }
        }
    }
    Ok(false)
}

// fast forward all content of an element and its end
#[cfg(feature = "xml")]
pub fn fast_forward_to_end<B: ::std::io::BufRead, C: ExtensionType>(
    reader: &mut XmlReader<B, C>,
) -> Result<(), protosdc_xml::ParserError> {
    let mut buf = vec![];
    let mut depth = 1;
    while depth > 0 {
        match reader.read_next(&mut buf) {
            Ok((ref _ns, quick_xml::events::Event::Start(ref _e))) => {
                depth += 1;
            }
            Ok((ref _ns, quick_xml::events::Event::End(ref _e))) => {
                depth -= 1;
            }
            Err(err) => Err(protosdc_xml::ParserError::QuickXMLError(err))?,
            _ => {}
        }
    }
    Ok(())
}

#[cfg(feature = "xml")]
pub fn parse_extension<B: ::std::io::BufRead>(
    tag_name: QNameSlice,
    _event: &quick_xml::events::BytesStart,
    reader: &mut protosdc_xml::XmlReader<B, Box<dyn AnyContent + Send + Sync + 'static>>,
) -> Result<crate::biceps::Extension, protosdc_xml::ParserError> {
    let mut buf = vec![];

    let mut extensions: Vec<crate::biceps::extension_mod::Item> = vec![];

    loop {
        match reader.read_next(&mut buf) {
            // don't care about starts
            Ok((ref ns, quick_xml::events::Event::Start(ref e))) => {
                match (ns, e.local_name().as_ref()) {
                    (
                        ResolveResult::Bound(Namespace(
                            b"http://standards.ieee.org/downloads/11073/11073-10207-2017/message",
                        )),
                        b"Retrievability",
                    ) => {
                        // determine mustUnderstand
                        let must_understand = find_must_understand(&e, reader)?;
                        let extension = crate::biceps::Retrievability::from_xml_complex((ResolveResult::Bound(Namespace(b"http://standards.ieee.org/downloads/11073/11073-10207-2017/message")), b"Retrievability"), e, reader)?;
                        extensions.push(crate::biceps::extension_mod::Item {
                            must_understand,
                            extension_data: ProtoAny {
                                data: Box::new(extension),
                            },
                        })
                    }
                    #[cfg(feature = "sdpi")]
                    (
                        ResolveResult::Bound(Namespace(
                            b"urn:oid:1.3.6.1.4.1.19376.1.6.2.10.1.1.1",
                        )),
                        b"CodedAttributes",
                    ) => {
                        let actual_extension =
                            crate::extension::sdpi::CodedAttributes::from_xml_complex(
                                (
                                    ResolveResult::Bound(Namespace(
                                        b"urn:oid:1.3.6.1.4.1.19376.1.6.2.10.1.1.1",
                                    )),
                                    b"CodedAttributes",
                                ),
                                e,
                                reader,
                            )?;
                        extensions.push(crate::biceps::extension_mod::Item {
                            must_understand: actual_extension
                                .coded_attributes
                                .must_understand_attr
                                .unwrap_or(false),
                            extension_data: ProtoAny {
                                data: Box::new(actual_extension),
                            },
                        })
                    }
                    (_other_ns, other_element) => {
                        // check if extension is must understand
                        let must_understand = find_must_understand(&e, reader)?;

                        // try parsing it using known extension types
                        let mut handler = std::mem::replace(
                            &mut reader.extension_handler,
                            Box::new(ReplacementExtensionHandler),
                        );
                        let res = handler.handle(e, reader);
                        reader.extension_handler = handler;

                        match (must_understand, res) {
                            (m_u, Ok(ext)) => extensions.push(crate::biceps::extension_mod::Item {
                                must_understand: m_u,
                                extension_data: ProtoAny { data: ext },
                            }),
                            (true, Err(_err)) => {
                                Err(protosdc_xml::ParserError::UnexpectedParserStartState {
                                    element_name: match String::from_utf8(
                                        e.local_name().as_ref().to_vec(),
                                    ) {
                                        Ok(it) => it,
                                        Err(_) => "FromUtf8Error".to_string(),
                                    },
                                    parser_state: "CustomExtension".to_string(),
                                })?
                            }
                            (false, Err(ExtensionParserError::UnknownElement { name })) => {
                                warn!("Encountered an unknown extension {}", name);
                                // fast forward
                                fast_forward_to_end(reader)?;
                            }
                            (false, Err(ExtensionParserError::ParserError(err))) => {
                                error!(
                                    "Encountered a broken extension {:?}",
                                    String::from_utf8_lossy(other_element)
                                );
                                Err(err)?;
                            }
                        }
                    }
                }
            }
            Ok((ref ns, quick_xml::events::Event::End(ref e))) => {
                match (ns, e.local_name()) {
                    (ns, local_name) if &tag_name.0 == ns && tag_name.1 == local_name.as_ref() => {
                        return Ok(crate::biceps::Extension { item: extensions })
                    }
                    // do not care about any other events right now
                    _ => {}
                }
            }
            Ok((_, quick_xml::events::Event::Empty(_))) => {}
            Ok((_, quick_xml::events::Event::Comment(_))) => {}
            Ok((_, quick_xml::events::Event::CData(_))) => {}
            Ok((_, quick_xml::events::Event::Decl(_))) => {}
            Ok((_, quick_xml::events::Event::PI(_))) => {}
            Ok((_, quick_xml::events::Event::DocType(_))) => {}
            Ok((_, quick_xml::events::Event::Text(_))) => {}
            Ok((_, quick_xml::events::Event::Eof)) => {
                Err(protosdc_xml::ParserError::UnexpectedEof)?
            }
            Err(err) => Err(protosdc_xml::ParserError::QuickXMLError(err))?,
        }
    }
}

#[cfg(feature = "xml")]
pub fn write_extension(
    extension: &crate::biceps::Extension,
    _tag_name: Option<QNameStr>,
    writer: &mut protosdc_xml::XmlWriter<Vec<u8>>,
    _xsi_type: bool,
) -> Result<(), protosdc_xml::WriterError> {
    if extension.item.is_empty() {
        return Ok(());
    }
    writer.write_start(
        Some("http://standards.ieee.org/downloads/11073/11073-10207-2017/extension"),
        "Extension",
    )?;

    for item in &extension.item {
        item.extension_data
            .data
            .to_xml_complex(None, writer, false)?;
    }

    writer.write_end(
        Some("http://standards.ieee.org/downloads/11073/11073-10207-2017/extension"),
        "Extension",
    )?;
    Ok(())
}

#[cfg(all(test, feature = "sdpi"))]
mod test {
    use crate::biceps::extension_mod::Item;
    use crate::biceps::Extension;
    use crate::extension::sdpi::{
        gender_type_mod, EquipmentIdentifier, EquipmentIdentifierType, Gender, GenderType,
    };
    use crate::types::{ProtoAny, ProtoUri};

    #[test]
    fn test_proto_any_equality() -> anyhow::Result<()> {
        let gender1 = Gender {
            gender_type: GenderType {
                enum_type: gender_type_mod::EnumType::Male,
            },
            must_understand_attr: None,
        };
        let gender2 = Gender {
            gender_type: GenderType {
                enum_type: gender_type_mod::EnumType::Other,
            },
            must_understand_attr: None,
        };

        let equipment_identifier1 = EquipmentIdentifier {
            equipment_identifier_type: EquipmentIdentifierType {
                any_u_r_i: ProtoUri {
                    uri: "abc".to_string(),
                },
            },
            must_understand_attr: None,
        };
        let equipment_identifier2 = EquipmentIdentifier {
            equipment_identifier_type: EquipmentIdentifierType {
                any_u_r_i: ProtoUri {
                    uri: "cba".to_string(),
                },
            },
            must_understand_attr: None,
        };

        {
            let extension1 = Extension {
                item: vec![Item {
                    must_understand: false,
                    extension_data: ProtoAny {
                        data: Box::new(gender1.clone()),
                    },
                }],
            };
            let extension2 = Extension {
                item: vec![Item {
                    must_understand: false,
                    extension_data: ProtoAny {
                        data: Box::new(gender1.clone()),
                    },
                }],
            };

            assert_eq!(extension1, extension2)
        }
        {
            let extension1 = Extension {
                item: vec![Item {
                    must_understand: false,
                    extension_data: ProtoAny {
                        data: Box::new(gender1.clone()),
                    },
                }],
            };
            let extension2 = Extension {
                item: vec![Item {
                    must_understand: false,
                    extension_data: ProtoAny {
                        data: Box::new(gender2.clone()),
                    },
                }],
            };

            assert_ne!(extension1, extension2)
        }
        {
            let extension1 = Extension {
                item: vec![Item {
                    must_understand: false,
                    extension_data: ProtoAny {
                        data: Box::new(gender1.clone()),
                    },
                }],
            };
            let extension2 = Extension {
                item: vec![Item {
                    must_understand: false,
                    extension_data: ProtoAny {
                        data: Box::new(equipment_identifier1.clone()),
                    },
                }],
            };

            assert_ne!(extension1, extension2)
        }

        {
            let extension1 = Extension {
                item: vec![
                    Item {
                        must_understand: false,
                        extension_data: ProtoAny {
                            data: Box::new(gender1.clone()),
                        },
                    },
                    Item {
                        must_understand: false,
                        extension_data: ProtoAny {
                            data: Box::new(equipment_identifier2.clone()),
                        },
                    },
                ],
            };
            let extension2 = Extension {
                item: vec![
                    Item {
                        must_understand: false,
                        extension_data: ProtoAny {
                            data: Box::new(gender1.clone()),
                        },
                    },
                    Item {
                        must_understand: false,
                        extension_data: ProtoAny {
                            data: Box::new(equipment_identifier2.clone()),
                        },
                    },
                ],
            };

            assert_eq!(extension1, extension2)
        }

        {
            let extension1 = Extension {
                item: vec![
                    Item {
                        must_understand: false,
                        extension_data: ProtoAny {
                            data: Box::new(gender1.clone()),
                        },
                    },
                    Item {
                        must_understand: false,
                        extension_data: ProtoAny {
                            data: Box::new(equipment_identifier2.clone()),
                        },
                    },
                ],
            };
            let extension2 = Extension {
                item: vec![
                    Item {
                        must_understand: false,
                        extension_data: ProtoAny {
                            data: Box::new(equipment_identifier2.clone()),
                        },
                    },
                    Item {
                        must_understand: false,
                        extension_data: ProtoAny {
                            data: Box::new(gender1.clone()),
                        },
                    },
                ],
            };

            assert_ne!(extension1, extension2)
        }

        Ok(())
    }
}
