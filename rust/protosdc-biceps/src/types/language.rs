use lazy_static::lazy_static;
use protosdc_mapping::{MappingError, ProtoSdcType};
use regex::Regex;
use std::ops::Deref;

lazy_static! {
    static ref LANGUAGE_VALIDATOR: Regex =
        Regex::new(r"[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*").unwrap();
}

#[derive(Debug, Clone, PartialEq)]
pub struct ProtoLanguage {
    pub language: String,
}

impl Deref for ProtoLanguage {
    type Target = String;
    fn deref(&self) -> &Self::Target {
        &self.language
    }
}

impl TryFrom<String> for ProtoLanguage {
    type Error = MappingError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match LANGUAGE_VALIDATOR.is_match(&value) {
            true => Ok(Self { language: value }),
            false => Err(MappingError::FromProtoGeneric {
                message: format!("String {} was not a valid language.", &value),
            }),
        }
    }
}

impl Into<String> for ProtoLanguage {
    fn into(self) -> String {
        self.language
    }
}

impl ProtoSdcType<String> for ProtoLanguage {}

#[cfg(test)]
mod test {
    use crate::types::ProtoLanguage;
    use anyhow::Result;
    use protosdc_mapping::MappingError;

    #[test]
    fn valid() -> Result<()> {
        let valid_data = vec!["en-US", "de-DE", "tlh"];

        for lang in valid_data {
            ProtoLanguage::try_from(lang.to_string())?;
        }
        Ok(())
    }

    #[test]
    fn invalid() -> Result<()> {
        let valid_data = vec!["🐻-✨", "1111111111"];

        for lang in valid_data {
            let result = ProtoLanguage::try_from(lang.to_string());
            assert!(matches!(result, Err(MappingError::FromProtoGeneric { .. })))
        }
        Ok(())
    }
}
