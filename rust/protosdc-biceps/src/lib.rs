pub mod biceps {
    mod out;

    pub use out::*;
}

pub mod extension {
    #[cfg(feature = "sdpi")]
    pub mod sdpi {
        mod out;
        pub use out::*;
    }
}

pub mod types;

#[cfg(feature = "xml")]
pub mod xml {
    mod parser {
        #![allow(warnings, unused)]
        mod out;

        pub use out::*;
    }
    mod writer {
        #![allow(warnings, unused)]
        mod out;

        pub use out::*;
    }

    mod extension {
        #[cfg(feature = "sdpi")]
        mod sdpi {
            mod parser {
                #![allow(warnings, unused)]

                mod out;

                pub use out::*;
            }

            mod writer {
                #![allow(warnings, unused)]

                mod out;

                pub use out::*;
            }
        }
    }
}
