#[cfg(all(feature = "xml"))]
mod test1 {
    use chrono::Datelike;
    use protosdc_biceps::biceps::patient_demographics_core_data_mod::DateOfBirth;
    use protosdc_biceps::biceps::LocalizedText;
    use protosdc_biceps::types::{AnyContent, DateEnum, DateTimeEnum};
    use protosdc_xml::{
        ExtensionHandler, ExtensionParserError, GenericXmlReaderComplexTypeRead, XmlReader,
    };
    use quick_xml::events::{BytesStart, Event};
    use quick_xml::name::{Namespace, ResolveResult};
    use std::io::BufRead;

    const PARTICIPANT_MODEL: &[u8] =
        b"http://standards.ieee.org/downloads/11073/11073-10207-2017/participant";

    #[derive(Clone, Debug)]
    struct FailingExtensionHandler;
    impl<B: BufRead> ExtensionHandler<B, Box<dyn AnyContent + Send + Sync>>
        for FailingExtensionHandler
    {
        fn handle(
            &mut self,
            _start: &BytesStart,
            _reader: &mut XmlReader<B, Box<dyn AnyContent + Send + Sync>>,
        ) -> Result<Box<dyn AnyContent + Send + Sync>, ExtensionParserError> {
            Err(ExtensionParserError::UnknownElement {
                name: format!("No extensions expected"),
            })
        }
    }

    #[test]
    fn parse_date_of_birth_variants() -> anyhow::Result<()> {
        {
            let data = "2002-10-10T09:00:00.050Z";
            let parsed = parse_date_of_birth(data)?;
            match parsed {
                DateOfBirth::DateTime(dt) => match dt.date_time {
                    DateTimeEnum::DateTime(dt2) => {
                        assert_eq!(2002, dt2.year())
                    }
                    _ => panic!("DateTime has TimeZone"),
                },
                _ => panic!("Incorrect type"),
            }
        }
        {
            let data = "2001-10-26";
            let parsed = parse_date_of_birth(data)?;
            match parsed {
                DateOfBirth::Date(dt) => match dt.date {
                    DateEnum::NaiveDate(date) => {
                        assert_eq!(2001, date.year());
                        assert_eq!(10, date.month());
                        assert_eq!(26, date.day());
                    }
                    _ => panic!("DateTime has TimeZone"),
                },
                _ => panic!("Incorrect type"),
            }
        }
        {
            let data = "2020-10";
            let parsed = parse_date_of_birth(data)?;
            match parsed {
                DateOfBirth::GYearMonth(dt) => {
                    assert_eq!(2020, dt.year);
                    assert_eq!(10, dt.month);
                }
                _ => panic!("Incorrect type"),
            }
        }
        {
            let data = "1996";
            let parsed = parse_date_of_birth(data)?;
            match parsed {
                DateOfBirth::GYear(dt) => {
                    assert_eq!(1996, dt.year);
                }
                _ => panic!("Incorrect type"),
            }
        }

        Ok(())
    }

    #[test]
    fn parse_localized_text_cdata() -> anyhow::Result<()> {
        let data = r#"<ConceptDescription xmlns="http://standards.ieee.org/downloads/11073/11073-10207-2017/participant" Lang="tlh"><![CDATA[qapla']]></ConceptDescription>"#;
        let parsed = parse_localized_text(data.as_bytes())?;

        assert_eq!("tlh", parsed.lang_attr.unwrap().language.as_str());
        assert_eq!("qapla'", parsed.localized_text_content.string);

        Ok(())
    }

    fn parse_localized_text(input: &[u8]) -> anyhow::Result<LocalizedText> {
        let mut reader = XmlReader::create_custom(input, Box::new(FailingExtensionHandler));

        let mut buf = vec![];

        let get_mdib_response: LocalizedText = loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    break LocalizedText::from_xml_complex(
                        (
                            ResolveResult::Bound(Namespace(PARTICIPANT_MODEL)),
                            b"ConceptDescription",
                        ),
                        e,
                        &mut reader,
                    )?;
                }
                Ok((ref _ns, Event::DocType(ref _e))) => {}
                Ok((ref _ns, Event::Decl(ref _e))) => {}
                Ok((ref _ns, Event::Comment(ref _e))) => {}
                _ => panic!("Could not parse localized text, unexpected initial event"),
            };
        };

        Ok(get_mdib_response)
    }

    fn parse_date_of_birth(payload: &str) -> anyhow::Result<DateOfBirth> {
        let input = format!("<pm:DateOfBirth xmlns:pm=\"http://standards.ieee.org/downloads/11073/11073-10207-2017/participant\">{}</pm:DateOfBirth", payload);

        let mut reader =
            XmlReader::create_custom(input.as_bytes(), Box::new(FailingExtensionHandler));

        let mut buf = vec![];

        let dob: DateOfBirth = loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    break DateOfBirth::from_xml_complex(
                        (
                            ResolveResult::Bound(Namespace(PARTICIPANT_MODEL)),
                            b"DateOfBirth",
                        ),
                        e,
                        &mut reader,
                    )?;
                }
                Ok((ref _ns, Event::DocType(ref _e))) => {}
                Ok((ref _ns, Event::Decl(ref _e))) => {}
                Ok((ref _ns, Event::Comment(ref _e))) => {}
                _ => panic!("Could not parse date of birth text, unexpected initial event"),
            };
        };

        Ok(dob)
    }
}

#[cfg(all(feature = "xml", feature = "sdpi"))]
mod test2 {
    use anyhow::Result as AnyhowResult;
    use prost_types::Any;
    use protosdc_biceps::biceps::{
        patient_demographics_core_data_mod, AbstractOperationDescriptorOneOf, AbstractStateOneOf,
        GetMdibResponse, PatientDemographicsCoreDataOneOf, QualifiedName,
    };
    use protosdc_biceps::types::{AnyContent, ProtoDateTime, ToProstAnyMessage};
    use protosdc_xml::xml_reader::ExtensionParserError;
    use protosdc_xml::{
        ComplexXmlTypeWrite, ExtensionHandler, GenericXmlReaderComplexTypeRead, NamespaceMapping,
        ParserError, QNameSlice, QNameSliceRef, QNameStr, WriterError, XmlReader, XmlWriter,
    };
    use quick_xml::events::{BytesStart, Event};
    use quick_xml::name::{Namespace, ResolveResult};
    use std::io::BufRead;

    //noinspection HttpUrlsUsage
    const MESSAGE_MODEL: &[u8] =
        b"http://standards.ieee.org/downloads/11073/11073-10207-2017/message";

    #[test]
    fn get_mdib_response_default_namespace() -> AnyhowResult<()> {
        let data = include_bytes!("mdib_default_namespace.xml");
        get_mdib_test(data)
    }

    #[test]
    fn get_mdib_response() -> AnyhowResult<()> {
        let data = include_bytes!("mdib.xml");
        get_mdib_test(data)
    }

    fn get_mdib_test(data: &[u8]) -> AnyhowResult<()> {
        let mdib: GetMdibResponse = parse_get_mdib(data)?;

        let mdib_extension_element = mdib.mdib.extension_element.as_ref().expect("No extension");
        assert!(!mdib_extension_element.item.is_empty());
        let mdib_extension = mdib_extension_element.item.get(0).expect("No item");
        (*(mdib_extension.extension_data.data))
            .as_any()
            .downcast_ref::<Box<MyGenericExtension>>()
            .expect("Incorrect extension present");

        assert!(mdib.mdib.md_description.is_some());

        let manufacture_date: String = mdib
            .mdib
            .md_description
            .as_ref()
            .unwrap()
            .mds
            .get(0)
            .unwrap()
            .meta_data
            .as_ref()
            .unwrap()
            .manufacture_date
            .as_ref()
            .unwrap()
            .clone()
            .into();

        let sco = mdib
            .mdib
            .md_description
            .as_ref()
            .unwrap()
            .mds
            .get(0)
            .unwrap()
            .abstract_complex_device_component_descriptor
            .sco
            .as_ref()
            .unwrap();

        let activate = sco.operation.get(0).unwrap();

        for operation in &sco.operation {
            assert!(!matches!(
                operation,
                AbstractOperationDescriptorOneOf::AbstractOperationDescriptor(..)
            ))
        }

        match activate {
            AbstractOperationDescriptorOneOf::ActivateOperationDescriptor(it) => {
                let arg = &it.argument.get(0).unwrap().arg;
                assert_eq!(
                    &QualifiedName {
                        namespace: "http://bad.idea".to_string(),
                        local_name: "knoedelarg".to_string()
                    },
                    arg
                )
            }
            _ => panic!("Wrong type {:?}", activate),
        }
        assert_eq!("2022-10-09T23:00:59Z", &manufacture_date);

        let patient_context_state = mdib
            .mdib
            .md_state
            .iter()
            .next()
            .unwrap()
            .state
            .iter()
            .find_map(|it| match it {
                AbstractStateOneOf::PatientContextState(patient) => Some(patient),
                _ => None,
            })
            .expect("No patient");

        let core_data = match patient_context_state.core_data.as_ref().unwrap() {
            PatientDemographicsCoreDataOneOf::PatientDemographicsCoreData(it) => it,
            _ => panic!(),
        };

        let expected_date_of_birth =
            ProtoDateTime::try_from("2002-10-10T09:00:00.050Z".to_string()).unwrap();

        assert_eq!(
            Some(patient_demographics_core_data_mod::DateOfBirth::DateTime(
                expected_date_of_birth
            )),
            core_data.date_of_birth
        );

        let mut writer = XmlWriter::new(vec![
            NamespaceMapping::new(
                "http://standards.ieee.org/downloads/11073/11073-10207-2017/participant"
                    .to_string(),
                "pm".to_string(),
            ),
            NamespaceMapping::new(
                "http://standards.ieee.org/downloads/11073/11073-10207-2017/message".to_string(),
                "msg".to_string(),
            ),
            NamespaceMapping::new(
                "http://standards.ieee.org/downloads/11073/11073-10207-2017/extension".to_string(),
                "ext".to_string(),
            ),
            NamespaceMapping::new(
                "http://www.w3.org/2001/XMLSchema-instance".to_string(),
                "xsi".to_string(),
            ),
        ]);

        mdib.to_xml_complex(None, &mut writer, false)?;

        let xml_again = String::from_utf8(writer.inner().clone()).expect("Xml");

        println!("{}", xml_again);

        Ok(())
    }

    const MY_GENERIC_EXTENSION_NAMESPACE: &str = "generic.extension";
    const MY_GENERIC_EXTENSION_NAME: &str = "MyGenericExtension";

    const MY_GENERIC_EXTENSION_TAG: QNameSlice = (
        ResolveResult::Bound(Namespace(MY_GENERIC_EXTENSION_NAMESPACE.as_bytes())),
        MY_GENERIC_EXTENSION_NAME.as_bytes(),
    );
    const MY_GENERIC_EXTENSION_TAG_REF: QNameSliceRef =
        (&MY_GENERIC_EXTENSION_TAG.0, MY_GENERIC_EXTENSION_TAG.1);

    #[derive(Clone, Debug, PartialEq)]
    struct MyGenericExtension {}

    impl ToProstAnyMessage for MyGenericExtension {
        fn to_prost(&self) -> Any {
            todo!("No need to implement, not converting this")
        }
    }

    impl GenericXmlReaderComplexTypeRead for MyGenericExtension {
        type Extension = Box<dyn AnyContent + Send + Sync>;

        fn from_xml_complex<B: BufRead>(
            _tag_name: QNameSlice,
            _event: &BytesStart,
            reader: &mut XmlReader<B, Self::Extension>,
        ) -> Result<Self, ParserError> {
            let mut buf = vec![];
            match reader.read_next(&mut buf) {
                Ok((ref ns, Event::End(ref e))) => match (ns, e.local_name().into_inner()) {
                    MY_GENERIC_EXTENSION_TAG_REF => return Ok(Self {}),
                    _ => Err(ParserError::UnexpectedParserEndState {
                        element_name: match String::from_utf8(e.local_name().into_inner().to_vec())
                        {
                            Ok(it) => it,
                            Err(_) => "FromUtf8Error".to_string(),
                        },
                        parser_state: "no state".to_string(),
                    })?,
                },
                Ok((ref _ns, other)) => Err(ParserError::UnexpectedParserEvent {
                    event: format!("{:?}", other),
                }),
                Err(err) => Err(ParserError::QuickXMLError(err))?,
            }
        }
    }

    impl ComplexXmlTypeWrite for MyGenericExtension {
        fn to_xml_complex(
            &self,
            tag_name: Option<QNameStr>,
            writer: &mut XmlWriter<Vec<u8>>,
            _xsi_type: bool,
        ) -> Result<(), WriterError> {
            let element_namespace = match tag_name {
                Some(it) => it.0,
                None => Some(MY_GENERIC_EXTENSION_NAMESPACE),
            };
            let element_tag = tag_name.map_or_else(|| MY_GENERIC_EXTENSION_NAME, |(_, it)| it);

            writer.write_start(element_namespace, element_tag)?;
            writer.write_end(element_namespace, element_tag)?;
            Ok(())
        }
    }

    #[derive(Clone, Debug)]
    struct MyExtensionHandler;
    impl<B: BufRead> ExtensionHandler<B, Box<dyn AnyContent + Send + Sync>> for MyExtensionHandler {
        fn handle(
            &mut self,
            start: &BytesStart,
            reader: &mut XmlReader<B, Box<dyn AnyContent + Send + Sync>>,
        ) -> Result<Box<dyn AnyContent + Send + Sync>, ExtensionParserError> {
            let resolved_tag = reader.resolve_element(start.name());

            match (resolved_tag.0, resolved_tag.1.into_inner()) {
                MY_GENERIC_EXTENSION_TAG => Ok(Box::new(Box::new(
                    MyGenericExtension::from_xml_complex(MY_GENERIC_EXTENSION_TAG, start, reader)?,
                ))),
                (a, b) => Err(ParserError::UnexpectedParserEvent {
                    event: format!("{:?} {:?}", a, b),
                })?,
            }
        }
    }

    fn parse_get_mdib(input: &[u8]) -> AnyhowResult<GetMdibResponse> {
        let mut reader = XmlReader::create_custom(input, Box::new(MyExtensionHandler));

        let mut buf = vec![];

        let get_mdib_response: GetMdibResponse = loop {
            match reader.read_next(&mut buf) {
                Ok((ref _ns, Event::Start(ref e))) => {
                    break GetMdibResponse::from_xml_complex(
                        (
                            ResolveResult::Bound(Namespace(MESSAGE_MODEL)),
                            b"GetMdibResponse",
                        ),
                        e,
                        &mut reader,
                    )?;
                }
                Ok((ref _ns, Event::DocType(ref _e))) => {}
                Ok((ref _ns, Event::Decl(ref _e))) => {}
                Ok((ref _ns, Event::Comment(ref _e))) => {}
                _ => panic!("Could not parse mdib, unexpected initial event"),
            };
        };

        Ok(get_mdib_response)
    }
}
