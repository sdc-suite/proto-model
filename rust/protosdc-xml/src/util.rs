use crate::expanded_name::ExpandedName;
use crate::{WriterError, XmlWriter};
use std::collections::HashMap;

pub fn write_attributes(
    writer: &mut XmlWriter<Vec<u8>>,
    attributes: &HashMap<ExpandedName, String>,
) -> Result<(), WriterError> {
    for (attribute_key, attribute_value) in attributes {
        writer.add_attribute_qname(
            attribute_key.namespace.as_deref(),
            &attribute_key.local_name,
            None,
            attribute_value,
        )?;
    }
    Ok(())
}

#[macro_export]
macro_rules! find_start_element {
    ($struct_name:ident, $qname:ident, $reader:ident, $buf:ident) => {{
        loop {
            match $reader.read_resolved_event_into(&mut $buf) {
                Ok((ref _ns, quick_xml::events::Event::Start(ref e))) => {
                    break $struct_name::from_xml_complex($qname, e, &mut $reader);
                }
                Ok((ref _ns, quick_xml::events::Event::DocType(ref _e))) => {}
                Ok((ref _ns, quick_xml::events::Event::Decl(ref _e))) => {}
                Ok((ref _ns, quick_xml::events::Event::Comment(ref _e))) => {}
                other => {
                    break Err($crate::ParserError::UnexpectedParserEvent {
                        event: format!("{:?}", other),
                    })
                }
            };
        }
    }};
}

// export macro crate-wide
pub use find_start_element;
