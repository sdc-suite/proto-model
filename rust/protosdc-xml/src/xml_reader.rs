use crate::{
    ComplexXmlTypeWrite, ExpandedName, ParserError, QNameSlice, QNameStr, WriterError, XmlWriter,
};
use dyn_clone::DynClone;
use quick_xml::events::{BytesStart, Event};
use quick_xml::name::{LocalName, QName, ResolveResult};
use quick_xml::{Decoder, NsReader};
use std::any::Any;
use std::collections::HashMap;
use std::fmt::Debug;
use std::io::BufRead;
use thiserror::Error;

fn create_reader<B: BufRead>(input: B) -> NsReader<B> {
    let mut reader = NsReader::from_reader(input);
    reader.trim_text(true);
    reader.expand_empty_elements(true);

    reader
}

pub trait ExtensionType: Any + ComplexXmlTypeWrite {
    fn as_any(&self) -> &dyn Any;
}

impl<T> ExtensionType for T
where
    T: Any + ComplexXmlTypeWrite + Sized,
{
    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl<T: ComplexXmlTypeWrite + ?Sized> ComplexXmlTypeWrite for Box<T> {
    fn to_xml_complex(
        &self,
        tag_name: Option<QNameStr>,
        writer: &mut XmlWriter<Vec<u8>>,
        xsi_type: bool,
    ) -> Result<(), WriterError> {
        (**self).to_xml_complex(tag_name, writer, xsi_type)
    }
}

pub trait ExtensionHandler<B: BufRead, C: ExtensionType>: Debug + DynClone {
    fn handle(
        &mut self,
        start: &BytesStart,
        reader: &mut XmlReader<B, C>,
    ) -> Result<C, ExtensionParserError>;
}

dyn_clone::clone_trait_object!(<B, C> ExtensionHandler<B, C> where B: BufRead, C: ExtensionType);

#[derive(Debug, Error)]
pub enum ExtensionParserError {
    #[error("The element {name} is an unknown extension")]
    UnknownElement { name: String },
    #[error(transparent)]
    ParserError(#[from] ParserError),
}

#[derive(Clone, Debug)]
pub struct DummyType {}

impl ComplexXmlTypeWrite for DummyType {
    fn to_xml_complex(
        &self,
        _tag_name: Option<QNameStr>,
        _writer: &mut XmlWriter<Vec<u8>>,
        _xsi_type: bool,
    ) -> Result<(), WriterError> {
        Err(WriterError::IllegalState {
            expected: "No extensions present".to_string(),
            actual: "extension present".to_string(),
        })
    }
}

#[derive(Clone, Debug)]
pub struct ReplacementExtensionHandler;

impl<B: BufRead, C: ExtensionType> ExtensionHandler<B, C> for ReplacementExtensionHandler {
    fn handle(
        &mut self,
        _: &BytesStart,
        _: &mut XmlReader<B, C>,
    ) -> Result<C, ExtensionParserError> {
        Err(ExtensionParserError::UnknownElement {
            name: format!("ReplacementExtensionHandler does not handle any element"),
        })?
    }
}

pub trait DummyExtensionTrait: ComplexXmlTypeWrite + ExtensionType {}
pub trait DummyExtensionTraitBox: DummyExtensionTrait + DynClone {}

pub struct XmlReader<B: BufRead, C: ExtensionType> {
    pub reader: NsReader<B>,
    pub extension_handler: Box<dyn ExtensionHandler<B, C> + Send + Sync>,
}

impl<B: BufRead> XmlReader<B, Box<dyn DummyExtensionTrait>> {
    /// Creates a new instance of the XmlReader.
    ///
    /// This function takes an object implementing the BufRead trait as parameter,
    /// which allows it to read from a variety of sources like Files, Sockets etc.
    ///
    /// The function creates an XmlReader with a `DummyExtensionTrait` handler for
    /// XML extension handling. The `DummyExtensionTrait` is boxed, which allows
    /// working with the `DummyExtensionTrait` trait object as a type.
    /// The `ReplacementExtensionHandler` is used with this function where no
    /// specific functionality is required as it replaces all handled extensions with nothing.
    ///
    /// If you require proper extension handling, create your own using `create_custom`
    ///
    /// # Parameters
    ///
    /// * `input` - An object implementing the BufRead trait that will be used as
    ///             the data source for the XmlReader.
    ///
    /// # Returns
    ///
    /// A new instance of XmlReader.
    ///
    /// # Examples
    ///
    /// ```
    /// use protosdc_xml::XmlReader;
    ///
    /// let data = "<a>b</a>";
    ///
    /// let xml_reader = XmlReader::create(data.as_bytes());
    /// ```
    pub fn create(input: B) -> XmlReader<B, Box<dyn DummyExtensionTrait>> {
        Self::create_custom(input, Box::new(ReplacementExtensionHandler))
    }
}

impl<B: BufRead, C: ExtensionType> XmlReader<B, C> {
    pub fn create_typed(input: B) -> XmlReader<B, C> {
        Self::create_custom(input, Box::new(ReplacementExtensionHandler))
    }

    pub fn create_custom(
        input: B,
        extension_handler: Box<dyn ExtensionHandler<B, C> + Send + Sync>,
    ) -> Self {
        Self {
            reader: create_reader(input),
            extension_handler,
        }
    }

    pub fn read_next<'b>(
        &mut self,
        buf: &'b mut Vec<u8>,
    ) -> Result<(ResolveResult, Event<'b>), quick_xml::Error> {
        self.reader.read_resolved_event_into(buf)
    }

    pub fn resolve_attribute<'n>(&self, name: QName<'n>) -> (ResolveResult, LocalName<'n>) {
        self.reader.resolve_attribute(name)
    }

    pub fn decoder(&self) -> Decoder {
        self.reader.decoder()
    }

    pub fn resolve_element<'n>(&self, name: QName<'n>) -> (ResolveResult, LocalName<'n>) {
        self.reader.resolve_element(name)
    }
}

pub trait XmlReaderSimpleXmlTypeRead: Sized {
    fn from_xml_simple<B: BufRead, C: ExtensionType>(
        data: &[u8],
        reader: &mut XmlReader<B, C>,
    ) -> Result<Self, ParserError>;
}
pub trait XmlReaderComplexXmlTypeRead: Sized {
    fn from_xml_complex<B: BufRead, C: ExtensionType>(
        tag_name: QNameSlice,
        event: &BytesStart,
        reader: &mut XmlReader<B, C>,
    ) -> Result<Self, ParserError>;
}

pub trait GenericXmlReaderSimpleTypeRead: Sized {
    type Extension: ExtensionType + ComplexXmlTypeWrite;
    fn from_xml_simple<B: BufRead>(
        data: &[u8],
        reader: &mut XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError>;
}

pub trait GenericXmlReaderComplexTypeRead: Sized {
    type Extension: ExtensionType + ComplexXmlTypeWrite;

    fn from_xml_complex<B: BufRead>(
        tag_name: QNameSlice,
        event: &BytesStart,
        reader: &mut XmlReader<B, Self::Extension>,
    ) -> Result<Self, ParserError>;
}

#[macro_export]
macro_rules! find_start_element_reader {
    ($struct_name:ident, $qname:ident, $reader:ident, $buf:ident) => {{
        loop {
            match $reader.read_next(&mut $buf) {
                Ok((ref _ns, quick_xml::events::Event::Start(ref e))) => {
                    break $struct_name::from_xml_complex($qname, e, &mut $reader);
                }
                Ok((ref _ns, quick_xml::events::Event::DocType(ref _e))) => {}
                Ok((ref _ns, quick_xml::events::Event::Decl(ref _e))) => {}
                Ok((ref _ns, quick_xml::events::Event::Comment(ref _e))) => {}
                other => {
                    break Err($crate::ParserError::UnexpectedParserEvent {
                        event: format!("{:?}", other),
                    })
                }
            };
        }
    }};
}

// export macro crate-wide
pub use find_start_element_reader;

pub fn collect_attributes_reader<B: BufRead, C: ExtensionType>(
    event: &BytesStart,
    reader: &mut XmlReader<B, C>,
) -> Result<HashMap<ExpandedName, String>, ParserError> {
    event
        .attributes()
        .flat_map(|it| {
            it.into_iter().map(|attribute| {
                // ignore xmlns attributes, they're namespace definitions
                if attribute.key.0.starts_with(b"xmlns:") || attribute.key.0 == b"xmlns" {
                    return Ok(None);
                }
                // handle qualified namespace
                let resolved = reader.resolve_attribute(attribute.key);

                let key: ExpandedName = resolved.try_into()?;

                let value_unescaped = attribute
                    .unescape_value()
                    .map_err(ParserError::QuickXMLError)?;
                let value = reader
                    .decoder()
                    .decode(value_unescaped.as_bytes())
                    .map_err(ParserError::QuickXMLError)?
                    .to_string();

                Ok(Some((key, value)))
            })
        })
        .filter_map(|it| match it {
            Err(err) => Some(Err(err)),
            Ok(data) => data.map(Ok),
        })
        .collect::<Result<HashMap<ExpandedName, String>, ParserError>>()
}
#[cfg(test)]
mod test {
    use crate::xml_reader::{
        collect_attributes_reader, ExtensionHandler, ExtensionParserError, ExtensionType,
        GenericXmlReaderComplexTypeRead, ReplacementExtensionHandler, XmlReader,
    };
    use crate::{
        ComplexXmlTypeWrite, ExpandedName, ParserError, QNameSlice, QNameSliceRef, QNameStr,
        WriterError, XmlWriter,
    };
    use dyn_clone::DynClone;
    use quick_xml::events::{BytesStart, Event};
    use quick_xml::name::{Namespace, ResolveResult};
    use std::any::TypeId;
    use std::collections::HashMap;
    use std::io::BufRead;

    #[derive(Clone, Debug)]
    struct A1 {
        a2: A2,
    }

    #[derive(Clone, Debug)]
    struct A2 {
        text: String,
    }

    const A1_NAME: &str = "a1";
    const A2_NAME: &str = "a2";

    const A1_NS: &str = "a1ns";
    const A2_NS: &str = "a2ns";
    const A1_TAG: QNameSlice = (
        ResolveResult::Bound(Namespace(A1_NS.as_bytes())),
        A1_NAME.as_bytes(),
    );
    const A2_TAG: QNameSlice = (
        ResolveResult::Bound(Namespace(A2_NS.as_bytes())),
        A2_NAME.as_bytes(),
    );
    const A2_TAG_REF: QNameSliceRef = (&A2_TAG.0, A2_TAG.1);

    pub trait ExtendedExtensionTrait: ComplexXmlTypeWrite + MyStrictTrait2 + ExtensionType {}
    pub trait ExtendedExtensionTraitBox: ExtendedExtensionTrait + DynClone {}

    impl<S: ComplexXmlTypeWrite + MyStrictTrait2 + 'static> ExtendedExtensionTrait for S {}

    impl<S: ExtendedExtensionTrait + Clone + ?Sized> ExtendedExtensionTraitBox for Box<S> {}

    dyn_clone::clone_trait_object!(ExtendedExtensionTraitBox);

    impl MyStrictTrait2 for A1 {
        fn strict_fun(&self) {}
    }

    impl<S: ExtendedExtensionTrait + ?Sized> MyStrictTrait2 for Box<S> {
        fn strict_fun(&self) {
            (**self).strict_fun()
        }
    }

    pub trait MyStrictTrait2 {
        fn strict_fun(&self);
    }

    impl GenericXmlReaderComplexTypeRead for A1 {
        type Extension = Box<dyn ExtendedExtensionTrait>;
        fn from_xml_complex<B: BufRead>(
            _tag_name: QNameSlice,
            event: &BytesStart,
            reader: &mut XmlReader<B, Self::Extension>,
        ) -> Result<Self, ParserError> {
            #[derive(Clone, Copy, Debug)]
            enum State {
                Start,
                A2End,
            }

            let resolved_tag = reader.resolve_element(event.name());

            let qname: ExpandedName = resolved_tag.try_into()?;

            // collect attributes
            let _attributes: HashMap<ExpandedName, String> =
                collect_attributes_reader(event, reader)?;

            let mut a2: Option<A2> = None;

            // read value
            let mut state = State::Start;
            let mut buf = vec![];
            loop {
                match reader.read_next(&mut buf) {
                    Ok((ref ns, Event::Start(ref e))) => {
                        // add more generic children we don't know
                        match (state, (ns, e.local_name().into_inner())) {
                            (State::Start, A2_TAG_REF) => {
                                a2 = Some(A2::from_xml_complex(A2_TAG, e, reader)?);
                                state = State::A2End;
                            }
                            _ => {
                                return Err(ParserError::UnexpectedParserStartState {
                                    element_name: match String::from_utf8(
                                        e.local_name().into_inner().to_vec(),
                                    ) {
                                        Ok(it) => it,
                                        Err(_) => "FromUtf8Error".to_string(),
                                    },
                                    parser_state: format!("2{:?}", state),
                                })?
                            }
                        }
                    }

                    Ok((ref ns, Event::End(ref e))) => {
                        match (state, ns, e.local_name().into_inner()) {
                            (_, ns, local_name)
                                if &qname.resolve_result() == ns
                                    && qname.local_name.as_bytes() == local_name =>
                            {
                                return Ok(Self {
                                    a2: a2.ok_or(ParserError::MandatoryElementMissing {
                                        element_name: A2_NAME.to_string(),
                                    })?,
                                });
                            }
                            _ => Err(ParserError::UnexpectedParserEndState {
                                element_name: match String::from_utf8(
                                    e.local_name().into_inner().to_vec(),
                                ) {
                                    Ok(it) => it,
                                    Err(_) => "FromUtf8Error".to_string(),
                                },
                                parser_state: format!("{:?}", state),
                            })?,
                        }
                    }

                    Ok((_, Event::Text(ref _e))) => match state {
                        _ => Err(ParserError::UnexpectedParserTextEventState {
                            parser_state: format!("{:?}", state),
                        })?,
                    },
                    Ok((_, Event::CData(_e))) => match state {
                        _ => Err(ParserError::UnexpectedParserTextEventState {
                            parser_state: format!("{:?}", state),
                        })?,
                    },
                    Ok((_, Event::Empty(_))) => {}
                    Ok((_, Event::Comment(_))) => {}
                    Ok((_, Event::Decl(_))) => {}
                    Ok((_, Event::PI(_))) => {}
                    Ok((_, Event::DocType(_))) => {}
                    Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                    Err(err) => Err(ParserError::QuickXMLError(err))?,
                }
            }
        }
    }

    impl GenericXmlReaderComplexTypeRead for A2 {
        type Extension = Box<dyn ExtendedExtensionTrait>;

        fn from_xml_complex<'a, B: BufRead>(
            _tag_name: QNameSlice,
            event: &'a BytesStart,
            reader: &'a mut XmlReader<B, Self::Extension>,
        ) -> Result<Self, ParserError> {
            #[derive(Clone, Copy, Debug)]
            enum State {
                Start,
            }

            let resolved_tag = reader.resolve_element(event.name());

            let qname: ExpandedName = resolved_tag.try_into()?;

            // collect attributes
            let _attributes: HashMap<ExpandedName, String> =
                collect_attributes_reader(event, reader)?;

            let mut text: Option<String> = None;

            // read value
            let state = State::Start;
            let mut buf = vec![];
            loop {
                match reader.read_next(&mut buf) {
                    Ok((ref _ns, Event::Start(ref e))) => {
                        // add more generic children we don't know
                        return Err(ParserError::UnexpectedParserStartState {
                            element_name: match String::from_utf8(
                                e.local_name().into_inner().to_vec(),
                            ) {
                                Ok(it) => it,
                                Err(_) => "FromUtf8Error".to_string(),
                            },
                            parser_state: format!("1{:?}", state),
                        })?;
                    }

                    Ok((ref ns, Event::End(ref e))) => {
                        match (state, ns, e.local_name().into_inner()) {
                            (_, ns, local_name)
                                if &qname.resolve_result() == ns
                                    && qname.local_name.as_bytes() == local_name =>
                            {
                                return Ok(Self {
                                    text: text.ok_or(ParserError::MandatoryContentMissing {
                                        element_name: A2_NAME.to_string(),
                                    })?,
                                });
                            }
                            _ => Err(ParserError::UnexpectedParserEndState {
                                element_name: match String::from_utf8(
                                    e.local_name().into_inner().to_vec(),
                                ) {
                                    Ok(it) => it,
                                    Err(_) => "FromUtf8Error".to_string(),
                                },
                                parser_state: format!("{:?}", state),
                            })?,
                        }
                    }

                    Ok((_, Event::Text(ref e))) => match state {
                        State::Start => {
                            text = Some(
                                e.unescape()
                                    .map_err(ParserError::QuickXMLError)?
                                    .to_string(),
                            );
                        }
                    },
                    Ok((_, Event::CData(_e))) => match state {
                        _ => Err(ParserError::UnexpectedParserTextEventState {
                            parser_state: format!("{:?}", state),
                        })?,
                    },
                    Ok((_, Event::Empty(_))) => {}
                    Ok((_, Event::Comment(_))) => {}
                    Ok((_, Event::Decl(_))) => {}
                    Ok((_, Event::PI(_))) => {}
                    Ok((_, Event::DocType(_))) => {}
                    Ok((_, Event::Eof)) => Err(ParserError::UnexpectedEof)?,
                    Err(err) => Err(ParserError::QuickXMLError(err))?,
                }
            }
        }
    }

    #[test]
    fn test_depth() -> anyhow::Result<()> {
        let test_text = r#"<a1:a1 xmlns:a1="a1ns" xmlns:a2="a2ns"><a2:a2>text</a2:a2></a1:a1>"#;
        {
            let mut reader =
                XmlReader::create_custom(test_text.as_bytes(), Box::new(AExtensionHandler {}));
            let mut buf = vec![];
            let start = find_start_element_reader!(A1, A1_TAG, reader, buf)?;

            assert_eq!("text", start.a2.text.as_str())
        }
        Ok(())
    }

    // implement write for A1 and A2
    impl ComplexXmlTypeWrite for A1 {
        fn to_xml_complex(
            &self,
            tag_name: Option<QNameStr>,
            writer: &mut XmlWriter<Vec<u8>>,
            _xsi_type: bool,
        ) -> Result<(), WriterError> {
            let element_namespace = match tag_name {
                Some(it) => it.0,
                None => Some(A1_NS),
            };
            let element_tag = tag_name.map_or_else(|| A1_NAME, |(_, it)| it);

            writer.write_start(element_namespace, element_tag)?;

            self.a2.to_xml_complex(None, writer, false)?;

            writer.write_end(element_namespace, element_tag)?;
            Ok(())
        }
    }
    impl ComplexXmlTypeWrite for A2 {
        fn to_xml_complex(
            &self,
            tag_name: Option<QNameStr>,
            writer: &mut XmlWriter<Vec<u8>>,
            _xsi_type: bool,
        ) -> Result<(), WriterError> {
            let element_namespace = match tag_name {
                Some(it) => it.0,
                None => Some(A2_NS),
            };
            let element_tag = tag_name.map_or_else(|| A2_NAME, |(_, it)| it);

            writer.write_start(element_namespace, element_tag)?;

            writer.write_text(self.text.as_str())?;

            writer.write_end(element_namespace, element_tag)?;
            Ok(())
        }
    }

    #[derive(Clone, Debug)]
    struct AExtensionHandler {}

    impl<B: BufRead> ExtensionHandler<B, Box<dyn ExtendedExtensionTrait>> for AExtensionHandler {
        fn handle(
            &mut self,
            start: &BytesStart,
            reader: &mut XmlReader<B, Box<dyn ExtendedExtensionTrait>>,
        ) -> Result<Box<dyn ExtendedExtensionTrait>, ExtensionParserError> {
            let resolved_tag = reader.resolve_element(start.name());

            match (resolved_tag.0, resolved_tag.1.into_inner()) {
                A1_TAG => Ok(Box::new(A1::from_xml_complex(A1_TAG, start, reader)?)),
                other => Err(ParserError::UnexpectedParserEvent {
                    event: format!("{:?}", other),
                })?,
            }
        }
    }

    fn handle_parse<B: BufRead>(
        reader: &mut XmlReader<B, Box<dyn ExtendedExtensionTrait>>,
    ) -> anyhow::Result<Box<dyn ExtendedExtensionTrait>> {
        let mut buf = vec![];
        match reader.read_next(&mut buf)? {
            (ref _ns, Event::Start(ref e)) => {
                let mut handler = std::mem::replace(
                    &mut reader.extension_handler,
                    Box::new(ReplacementExtensionHandler {}),
                );
                let res = handler.handle(e, reader);
                reader.extension_handler = handler;
                Ok(res?)
            }
            _ => panic!("Unknown branch"),
        }
    }

    #[test]
    fn test_extension_parsing() -> anyhow::Result<()> {
        let test_text = r#"<a1:a1 xmlns:a1="a1ns" xmlns:a2="a2ns"><a2:a2>text</a2:a2></a1:a1>"#;
        let mut reader =
            XmlReader::create_custom(test_text.as_bytes(), Box::new(AExtensionHandler {}));
        let parsed = handle_parse(&mut reader)?;

        assert_eq!(TypeId::of::<A1>(), (*parsed).as_any().type_id());

        let downcast = (*parsed)
            .as_any()
            .downcast_ref::<A1>()
            .expect("Could not downcast to A1");
        assert_eq!("text", &downcast.a2.text);

        Ok(())
    }
}
