use crate::xml_reader::{XmlReader, XmlReaderSimpleXmlTypeRead};
use crate::{ExtensionType, ParserError};
use quick_xml::name::{LocalName, Namespace, QName, ResolveResult};
use std::fmt::{Display, Formatter};
use std::io::BufRead;

pub const XML_NAMESPACE: &str = "http://www.w3.org/XML/1998/namespace";
pub const XML_NAMESPACE_PREFIX: &str = "xml";
pub const XML_NAMESPACE_PREFIX_BYTES: &[u8] = XML_NAMESPACE_PREFIX.as_bytes();

/// `ExpandedName` is a representation of the full name of an XML element or attribute.
///
/// The term is defined in Namespaces in XML 1.0 (Third Edition) and represents a local name
/// together with the URI that defines its namespace.
///
/// # Fields
/// `namespace`: The namespace URI associated with the XML term. `None` if the term has no namespace.
/// `local_name`: The local name of the XML term.
///
/// # Examples
///
/// ```
/// # use protosdc_xml::ExpandedName;
///
/// ExpandedName {
///     namespace: Some("http://www.w3.org/2000/xmlns/".to_string()),
///     local_name: "element".to_string(),
/// };
/// ```
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct ExpandedName {
    pub namespace: Option<String>,
    pub local_name: String,
}

impl Display for ExpandedName {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{{{}}}:{})",
            self.namespace.as_ref().unwrap_or(&"".to_string()),
            &self.local_name
        )
    }
}

impl ExpandedName {
    pub fn resolve_result(&self) -> ResolveResult {
        match &self.namespace {
            None => ResolveResult::Unbound,
            Some(it) => ResolveResult::Bound(Namespace(it.as_bytes())),
        }
    }
}

impl XmlReaderSimpleXmlTypeRead for ExpandedName {
    fn from_xml_simple<B: BufRead, C: ExtensionType>(
        data: &[u8],
        reader: &mut XmlReader<B, C>,
    ) -> Result<Self, ParserError> {
        reader.resolve_element(QName(data)).try_into()
    }
}

impl TryFrom<(Option<&[u8]>, &[u8])> for ExpandedName {
    type Error = ParserError;

    fn try_from(value: (Option<&[u8]>, &[u8])) -> Result<Self, Self::Error> {
        let (resolved_namespace, local_name) = value;
        let key = match resolved_namespace {
            None => ExpandedName {
                namespace: None,
                local_name: std::str::from_utf8(local_name)
                    .map(|it| it.to_string())
                    .map_err(|_err| ParserError::Other)?,
            },
            Some(namespace) => ExpandedName {
                namespace: Some(
                    std::str::from_utf8(namespace)
                        .map(|it| it.to_string())
                        .map_err(|_err| ParserError::Other)?,
                ),
                local_name: std::str::from_utf8(local_name)
                    .map(|it| it.to_string())
                    .map_err(|_err| ParserError::Other)?,
            },
        };
        Ok(key)
    }
}

impl TryFrom<(ResolveResult<'_>, LocalName<'_>)> for ExpandedName {
    type Error = ParserError;

    fn try_from(value: (ResolveResult, LocalName)) -> Result<Self, Self::Error> {
        let (resolved_namespace, local_name) = value;
        let key = match resolved_namespace {
            ResolveResult::Unbound => ExpandedName {
                namespace: None,
                local_name: std::str::from_utf8(local_name.into_inner())
                    .map(|it| it.to_string())
                    .map_err(|_err| ParserError::Other)?,
            },
            ResolveResult::Bound(Namespace(namespace)) => ExpandedName {
                namespace: Some(
                    std::str::from_utf8(namespace)
                        .map(|it| it.to_string())
                        .map_err(|_err| ParserError::Other)?,
                ),
                local_name: std::str::from_utf8(local_name.into_inner())
                    .map(|it| it.to_string())
                    .map_err(|_err| ParserError::Other)?,
            },
            ResolveResult::Unknown(unk) => match unk.as_slice() {
                // hardcode the xml prefix
                XML_NAMESPACE_PREFIX_BYTES => ExpandedName {
                    namespace: Some(XML_NAMESPACE.to_string()),
                    local_name: std::str::from_utf8(local_name.into_inner())
                        .map(|it| it.to_string())
                        .map_err(|_err| ParserError::Other)?,
                },
                _ => Err(ParserError::UnexpectedParserEvent {
                    event: format!("{{ResolveResult::Unknown({:?})}}, {:?}", unk, local_name),
                })?,
            },
        };
        Ok(key)
    }
}
