pub mod common {
    // used because of intellij completion
    include!(concat!(
        env!("OUT_DIR"),
        "/org.somda.protosdc.proto.model.common.rs"
    ));
}

pub mod biceps {
    // used because of intellij completion
    include!(concat!(
        env!("OUT_DIR"),
        "/org.somda.protosdc.proto.model.biceps.rs"
    ));
}

pub mod addressing {
    // used because of intellij completion
    include!(concat!(
        env!("OUT_DIR"),
        "/org.somda.protosdc.proto.model.addressing.rs"
    ));
}

pub mod metadata {
    // used because of intellij completion
    include!(concat!(
        env!("OUT_DIR"),
        "/org.somda.protosdc.proto.model.metadata.rs"
    ));
}

#[allow(unused_imports)]
pub mod discovery {
    use super::addressing;
    use super::common;

    // used because of intellij completion
    include!(concat!(
        env!("OUT_DIR"),
        "/org.somda.protosdc.proto.model.discovery.rs"
    ));
}

#[allow(unused_imports)]
pub mod sdc {
    use super::addressing;
    use super::biceps;

    // used because of intellij completion
    include!(concat!(
        env!("OUT_DIR"),
        "/org.somda.protosdc.proto.model.rs"
    ));
}

pub mod extension {
    #[cfg(feature = "sdpi")]
    pub mod sdpi {
        #[allow(unused_imports)] // used by include
        use crate::biceps;
        // used because of intellij completion
        include!(concat!(
            env!("OUT_DIR"),
            "/org.somda.protosdc.proto.model.extension.sdpi.rs"
        ));
    }
}
