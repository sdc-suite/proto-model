# protosdc-{biceps, mapping, mapping-derive, proto, xml}

## Summary
protosdc-biceps, protosdc-mapping, protosdc-mapping-derive, protosdc-proto, and protosdc-xml are part of an exploratory 
effort to implement the following standards using gRPC:

- IEEE 11073-10207: Point-of-care medical device communication Part 10207: Domain Information and Service Model for Service-Oriented Point-of-Care Medical Device Communication
- IEEE 11073-20701: Point-of-care medical device communication Part 20701: Service-Oriented Medical Device Exchange Architecture and Protocol Binding

They will be used in a future library which implements the functionality commonly required by SDC.

**They are not intended to be used in clinical trials, clinical studies, or in clinical routine.**

# License
protosdc-biceps, protosdc-mapping, protosdc-mapping-derive, protosdc-proto, and protosdc-xml are licensed under the MIT license, see [LICENSE](LICENSE) file.
