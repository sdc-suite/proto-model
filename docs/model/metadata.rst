########
Metadata
########

Getting Endpoint information and Metadata
#########################################

Once the physical address of a :term:`Service Provider` is known, a :term:`Service Consumer` may request :term:`Endpoint` information and :term:`Metadata` by using `MetadataService.GetMetadata()`.

.. note::

    * `MetadataService` is a gRPC service
    * GetMetadata can be used to verify correctness of a :term:`Hello` or :term:`SearchRequest` conveyed over an unsecured channel in :term:`Ad hoc Mode`

.. mermaid::

    sequenceDiagram
        Service Consumer->>+Service Provider: MetadataService.GetMetadata(GetMetadataRequest)
        Service Provider-->>-Service Consumer: GetMetadataResponse